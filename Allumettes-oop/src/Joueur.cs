namespace Allumettes
{
    abstract class Joueur
    {
        public string Name { get; set; }
        public Joueur(string name) {
            Name = name;
        }
        public abstract int Jouer(int allumettesRestantes);
    }

    class JoueurIaRandom : Joueur
    {
        public JoueurIaRandom(string name): base(name) {}
        public override int Jouer(int allumettesRestantes)
        {
            return new Random().Next(1, Math.Min(3, allumettesRestantes - 1) + 1);
        }
    }

    class JoueurHumain : Joueur
    {
        public JoueurHumain(string name): base(name) {}
        public override int Jouer(int allumettesRestantes)
        {
            int? allumettesARetirer;
            do
            {
                Console.Write($"{Name}, combien d'allumettes voulez-vous retirer ([1,{Math.Min(3, allumettesRestantes)}]) ? ");
                allumettesARetirer = int.Parse(Console.ReadLine());
            } while (allumettesARetirer < 1 || allumettesARetirer > Math.Min(3, allumettesRestantes - 1));
            return allumettesARetirer.Value;
        }
    }
}