namespace Allumettes
{
    class Partie
    {
        public int AllumettesRestantes { get; private set; }
        public int JoueurCourant { get; private set; }
        public Joueur[] Joueurs { get; private set; }

        public Partie(int allumettesInitiales, Joueur joueur1, Joueur joueur2)
        {
            AllumettesRestantes = allumettesInitiales;
            JoueurCourant = 0;
            Joueurs = new Joueur[] { joueur1, joueur2 };
        }

        public void Demarrer()
        {
            do
            {
                // affichage des allumettes
                for (int i = 0; i < AllumettesRestantes; ++i)
                    Console.Write("|");
                Console.WriteLine();
                // faire jouer le joueur courant
                int allumettesPrises = Joueurs[JoueurCourant].Jouer(AllumettesRestantes);
                AllumettesRestantes -= allumettesPrises;
                Console.WriteLine($"{Joueurs[JoueurCourant].Name} a pris {allumettesPrises} allumettes");
                // changer de joueur
                JoueurCourant = (JoueurCourant == 0) ? 1 : 0;
            } while (AllumettesRestantes > 1);
            // affichage message de fin de partie
            Console.WriteLine($"Le joueur {Joueurs[JoueurCourant].Name} a perdu !");
        }

    }
}

