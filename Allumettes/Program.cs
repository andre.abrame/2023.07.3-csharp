﻿
int allumettesRestantes = 10 + new Random().Next(5);
int joueurCourant = 0;

string[] nomDesJoueurs = new string[2];
string[] typeDesJoueurs = new string[2];

// choix des joueurs
for (int i=0; i<2; ++i) {
    Console.Write($"nom du joueur {i} ? ");
    nomDesJoueurs[i] = Console.ReadLine();
    Console.Write($"type du joueur {i} (ia ou humain) ? ");
    do {
        typeDesJoueurs[i] = Console.ReadLine();
    } while (typeDesJoueurs[i] != "ia" && typeDesJoueurs[i] != "humain");
}

// boucle principale
do
{
    for (int i = 0; i< allumettesRestantes; ++i)
        Console.Write("|");
    Console.WriteLine();
    // faire jouer le joueur courant
    allumettesRestantes -= typeDesJoueurs[joueurCourant] switch {
        "ia" => joueurIA(joueurCourant, allumettesRestantes),
        "humain" => joueurHumain(joueurCourant, allumettesRestantes),
    };
    // changer de joueur
    joueurCourant = (joueurCourant == 0) ? 1 : 0;
} while (allumettesRestantes > 1);

// affichage message de fin de partie
Console.WriteLine($"Le joueur {nomDesJoueurs[joueurCourant]} a perdu !");




static int joueurHumain(int numJoueur, int restantes) {
    int? allumettesARetirer;
    do {
        Console.Write($"joueur {numJoueur} : nombre d'allumettes à retirer (entre 1 et {Math.Min(3, restantes)}) ? ");
        allumettesARetirer = int.Parse(Console.ReadLine());
    } while (allumettesARetirer < 1 || allumettesARetirer > Math.Min(3, restantes-1));
    return allumettesARetirer.Value;
}

static int joueurIA(int numJoueur, int restantes) {
    int allumettesARetirer = new Random(restantes).Next(1, Math.Min(3, restantes-1)+1);
    Console.Write($"joueur {numJoueur} : nombre d'allumettes à retirer (entre 1 et {Math.Min(3, restantes-1)}) ? {allumettesARetirer}");
    return allumettesARetirer;
}