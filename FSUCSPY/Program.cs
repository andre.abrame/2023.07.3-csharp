﻿using System.Text.Json;
using arcelormittal.tab.finisseur;

string content;
using (StreamReader reader = new StreamReader("configOp.json"))
{
    content = reader.ReadToEnd();
}
Console.WriteLine(content);
ConfigOpArroseur config = JsonSerializer.Deserialize<ConfigOpArroseur>(content);


Intercage intercage1 = new Intercage();

RampeDecalaminage prsb1 = new RampeDecalaminage(
    TypeArroseur.SYMETRIQUE,
    intercage1,
    config
);

Produit produit = new Produit(){
    Pr = new List<int>(){123, 456, 70},
    Epaisseur = 220,
    Temperature = 999
};

prsb1.calculerConfig(produit);



Console.WriteLine(JsonSerializer.Serialize(prsb1, new JsonSerializerOptions{ WriteIndented = true}));