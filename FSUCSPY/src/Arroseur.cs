
using System.Linq.Expressions;

namespace arcelormittal.tab.finisseur {

    enum TypeArroseur {
        SYMETRIQUE,
        ASSYMETRIQUE
    }


    abstract class Arroseur
    {
        public TypeArroseur Type { get; set; }
        public ConfigArroseur Resultat { get; set; }
        public ConfigOpArroseur ConfigOp { get; set; }
        public Intercage Intercage { get; set; }

        public Arroseur(TypeArroseur type, Intercage intercage, ConfigOpArroseur configOp) {
            Type = type;
            Intercage = intercage;
            ConfigOp = configOp;
            Resultat = new ConfigArroseur();
        }

        public abstract void calculerConfig(Produit produit);
    }

    class RampeDecalaminage : Arroseur
    {
        public RampeDecalaminage(TypeArroseur type, Intercage intercage, ConfigOpArroseur configOp) : base(type, intercage, configOp) {}

        public override void calculerConfig(Produit produit)
        {
            Resultat.Actif = ConfigOp.Authorise && !produit.Pr.Contains(60) && !produit.Pr.Contains(160);
            // if (!ConfigOp.Authorise)
            //     Resultat.Actif = false;
            // else if (produit.Pr.Contains(60) || produit.Pr.Contains(160))
            //     Resultat.Actif = false;
            // else
            //     Resultat.Actif = true;

            if (ConfigOp.Retard.HasValue)
                Resultat.Retard = ConfigOp.Retard.Value;
            else if (produit.Temperature < 1000)
            {
                Resultat.Retard = 10;
            }
        }
    }

}