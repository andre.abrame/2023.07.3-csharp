
namespace arcelormittal.tab.finisseur {

    enum CoteArroseurAssymetrique {
        SUP,
        INF,
        SYM
    }

    class ConfigArroseur {
        public bool Actif { get; set; }
        public int Retard { get; set;}

        override public string ToString() {
            return $"Actif: {Actif} Retard: {Retard}";
        }
    }

    class ConfigArroseurAssymetrique: ConfigArroseur
    {
        public CoteArroseurAssymetrique cote { get; set; }
    }

}