
namespace arcelormittal.tab.finisseur {

    class ConfigOpArroseur
    {
        public bool Authorise { get; set; }
        public int? Retard { get; set; }
    }
    class ConfigOpArroseurAssymetrique: ConfigOpArroseur
    {
        public CoteArroseurAssymetrique CoteAuthorise { get; set; }
    }
}

