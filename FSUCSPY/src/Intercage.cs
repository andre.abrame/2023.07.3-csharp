namespace arcelormittal.tab.finisseur
{
    
    class Intercage
    {
        public List<Arroseur> Arroseurs { get; set; }
        public Cage Origine { get; set; }
        public Cage Destination { get; set; }
        public double tempsEntreBandes { get; set; }
    }
}