
namespace arcelormittal.tab.finisseur {

    class Produit
    {
        public List<int> Pr { get; set; }
        public double Epaisseur { get; set; }
        public double Temperature { get; set; }
    }
}