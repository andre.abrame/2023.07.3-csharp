﻿const int MAX = 9;

int resultat = new Random().Next(MAX + 1);
int? essai;
do
{
    // lecture de l'essai
    do
    {
        Console.Write("essai ? ");
        essai = int.Parse(Console.ReadLine());
        if (essai < 0 || essai > MAX)
            Console.Write($"doit être entre 0 et {MAX}. ");
    } while (essai < 0 || essai > MAX);

    // affichage message avec un switch
    switch (essai - resultat)
    {
        case < 0: Console.WriteLine("trop petit ..."); break;
        case > 0: Console.WriteLine("trop grand ..."); break;
        default: Console.WriteLine("bravo !"); break;
    }
    // affichage message avec un switch
    Console.WriteLine((essai - resultat) switch
    {
        < 0 => "trop petit ...",
        > 0 => "trop grand ...",
        _ => "bravo !"
    });
    // affichage message avec if ... else
    if (essai < resultat)
        Console.WriteLine("trop petit ...");
    else if (essai > resultat)
        Console.WriteLine("trop grand ...");
    else
        Console.WriteLine("bravo !");
} while (resultat != essai);