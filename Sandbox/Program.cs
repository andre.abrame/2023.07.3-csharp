﻿

using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using models;


// // dictionnaires

// Dictionary<int, string> fcb = new Dictionary<int, string>();
// fcb.Add(10, "messi");
// fcb.Add(23, "umtiti");
// fcb.Add(4, "Rakitic");
// fcb.Add(9, "Suarez");

// Console.Write("numero ? ");
// int numero = int.Parse(Console.ReadLine());
// Console.WriteLine(fcb.ContainsKey(numero) ? fcb[numero] : "not found");



List<string> voitures = new(){ "Citroen", "Ford", "Peugeot", "Mercedes" };

string? shortest = null;
foreach (var item in voitures)
{
    if (shortest == null || item.Length < shortest.Length)
    {
        shortest = item;
    }
}
Console.WriteLine(shortest);

string test = "andre";
voitures.Aggregate((shortest, current) => shortest.Length <= current.Length ? shortest : test);

Console.WriteLine(voitures
    .Select(item => item.Length)
    .OrderByDescending(item => item)
    .Aggregate((sum, item) => sum + item));
    


// var list = new List<string>{"a", "b", "c", "d"};
// var list = new List<int>{1, 2, 3, 4, 5, 6,};



// MyPredicate p1 = e => e%2 == 0;

// RemoveIf(list, IsEven);


// void RemoveIf(Collection<int> collection, Predicate<int> predicate) {
//     foreach (var item in collection)
//     {
//         if (predicate(item))
//             collection.Remove(item);
//     }
// }

// bool IsEven(int x) { return x % 2 == 0;}


// delegate bool MyPredicate(int element);


// var array = new ArrayList{ 2, 3, 5, 8, 2, 4, 2 };
// var positions = new ArrayList();
// var pairs = new ArrayList();

// // Exercice 1
// var firstOccurenceIndice = array.IndexOf(2);
// var secondOccurenceIndice = array.IndexOf(2, firstOccurenceIndice+1);



// // Exercice 2
// for (int i=0; i<array.Count; ++i) {
//     if ((int)array[i] == 2)
//         positions.Add(i);
// }
// int last = -1;
// while ((last = (int)array.IndexOf(2, last+1)) != -1)
//     positions.Add(last);


// // Exercice 3
// foreach (var item in array)
//     if ((int)item%2 == 0)
//         pairs.Add(item);
