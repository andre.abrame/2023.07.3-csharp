
namespace models {
    abstract class Animal
    {
        public abstract string Name { get; set; }
        
        public abstract void Eat();
    }

    abstract class Dog : Animal
    {
        // public override string Name { get; set; }
        public override void Eat()
        {
            Console.WriteLine($"The dog {Name} is eating.");
        }
        public void Bark()
        {
            Console.WriteLine($"{Name} is barking.");
        }
    }
}