

namespace models
{
    public class Cage
    {
        private int id;
        public int Id { get => id; set => id = (value > 0) ? value : 0; }
        public string Name { get; set; } = "";

        public Cage()
        {
            Id = 0;
            Name = "f1";
        }
        public Cage(int id)
        {
            Id = id;
            Name = $"f{id}";
        }
        public Cage(int id, string name)
        {
            Id = id;
            Name = name;
        }
        public Cage(string name)
        {
            Id = 0;
            Name = name;
        }

        public override string ToString()
        {
            return $"Cage id={this.Id} name={Name}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return this.Id == ((Cage)obj).Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }


    }

    public class CageQuarto : Cage {

        public int CylindreSoutient { get; set; }
        public CageQuarto(int cyclindres): base(789, "z0") {
            CylindreSoutient = cyclindres;
        }
    }

}