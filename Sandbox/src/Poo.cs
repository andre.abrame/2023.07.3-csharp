
// Person p = new Person(){
//     Id = 789,
//     Name = "andre"
// };

// Address a1 = new Address(){ Id = 123, City= "fos", Owner=p};
// Address a2 = new Address(){ Id = 456, City= "marseille", Owner=p};
// p.Address = new Address[]{a1, a2};

// Console.WriteLine(p.Address.FirstOrDefault(toto => toto.City.StartsWith("m")).City);


class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public Address[] Address { get; set; }
}

class Address
{
    public int Id { get; set; }
    public string City { get; set; }
    public Person Owner { get; set; }
}
