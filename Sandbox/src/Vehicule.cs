namespace models
{
    public interface VehiculeTerrestre {
        public void avancer();
    }
    public interface VehiculeMaritime {
        public void naviguer();
    }
    public class Voituer: VehiculeTerrestre {
        public void avancer() {
            Console.WriteLine("voituer avance : vroum");
        }
    }
    public class Velo: VehiculeTerrestre {
        public void avancer() {
            Console.WriteLine("velo avance : paf");
        }
    }
    public class Canoe : VehiculeTerrestre
    {
        public void avancer()
        {
             Console.WriteLine("canoe avance : flic floc");
        }
    }
    public class VoitureAmphibie: VehiculeTerrestre, VehiculeMaritime {
        public void avancer() {
            Console.WriteLine("VoitureAmphibie avance : vroum");
        }
        public void naviguer() {
            Console.WriteLine("VoitureAmphibie navigue : vroum");
        }
    }
}