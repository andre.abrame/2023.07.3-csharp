﻿

using Exercice1;
using Exercice2;

Point p1 = new Point(5, 2);
Point p2 = new Point(0, 0);

Console.WriteLine($"> 1.3 constructeur : {p1}");
Console.WriteLine($"> 1.4 calculerDistance : {p1.CalculerDistance(p2)}");
Console.WriteLine($"> 1.5 distance : {Point.Distance(1,2,5,-2)}");
Console.WriteLine($"> 1.6 milieu : {p1.CalculerMilieu(p2)}");

TroisPoints tp1 = new TroisPoints(new Point(-2,-2), new Point(0,0), new Point(5,5));
TroisPoints tp2 = new TroisPoints(new Point(2,-2), new Point(0,0), new Point(2,2));
Console.WriteLine($"> 1.9 sontAlignes :\n\t- {tp1} : {tp1.SontAlignes()}");
Console.WriteLine($"\t- {tp2} : {tp2.SontAlignes()}");
Console.WriteLine($"> 1.10 estIsocele :\n\t- {tp1} : {tp1.EstIsocele()}");
Console.WriteLine($"\t- {tp2} : {tp2.EstIsocele()}");

Formation formation = new Formation("C#", 35, new Stagiaire[]{
    new Stagiaire("samuel", new double[]{18, 14, 16}),
    new Stagiaire("alexandra", new double[]{10, 14, 19}),
    new Stagiaire("nicolas", new double[]{18, 16, 12}),
});

Console.WriteLine($"> 2.6 calulerMoyenneFormation : {formation.CalculerMoyenneFormation()}");
Console.WriteLine($"> 2.7 trouverIndiceMax : {formation.TrouverIndiceMax()} du texte .. {formation.Stagiaires[formation.TrouverIndiceMax()].Nom}");