using System.Security.Cryptography.X509Certificates;

namespace Exercice1
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double CalculerDistance(Point other)
        {
            return Math.Sqrt(Math.Pow(this.X - other.X, 2) + Math.Pow(this.Y - other.Y, 2));
        }

        public static double Distance(double x1, double y1, double x2, double y2)
        {
            // return new Point(x1, y1).CalculerDistance(new Point(x2, y2));
            Point a = new Point(x1, y1);
            Point b = new Point(x2, y2);
            return a.CalculerDistance(b);
        }

        public Point CalculerMilieu(Point other)
        {
            // this <=> (x1,y1) et other <=> (x2,y2)
            // calculer xm = (x1+x2)/2
            double xm = (this.X + other.X) / 2;
            // calculer ym = (y1+y2)/2
            double ym = (this.Y + other.Y) / 2;
            // créer un nouveau point (xm,ym)
            Point middle = new Point(xm, ym);
            // renvoyer ce point
            return middle;
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }


    class TroisPoints
    {
        public Point A { get; set; }
        public Point B { get; set; }
        public Point C { get; set; }

        public TroisPoints(Point a, Point b, Point c)
        {
            A = a;
            B = b;
            C = c;
        }

        public bool SontAlignes()
        {
            // calculer les distances AB, AC et BC (avec la méthode CalculerDistance)
            double AB = A.CalculerDistance(B);
            double AC = A.CalculerDistance(C);
            double BC = B.CalculerDistance(C);
            // si AB = AC + BC ou AC = AB + BC ou BC = AC + AB alors on renvoie true sinon false
            return AB == AC + BC || AC == AB + BC || BC == AC + AB;
        }
        
        public bool EstIsocele()
        {
            // calculer les distances AB, AC et BC (avec la méthode CalculerDistance)
            double AB = A.CalculerDistance(B);
            double AC = A.CalculerDistance(C);
            double BC = B.CalculerDistance(C);
            // si AB = AC ou AB = BC ou BC = AC alors on renvoie true sinon false
            return AB == AC || AB == BC || BC == AC;
        }

        override public string ToString() {
            return $"[{A},{B},{C}]";
        }
    }


}