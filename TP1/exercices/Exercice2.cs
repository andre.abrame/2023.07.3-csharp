using System.ComponentModel.DataAnnotations;

namespace Exercice2
{

    class Stagiaire
    {
        public string Nom { get; set; }
        public double[] Notes { get; set; }
        public Stagiaire(string nom, double[] notes)
        {
            Nom = nom;
            Notes = notes;
        }
        public double? CalculerMoyenne()
        {
            double sum = 0;
            foreach (var note in Notes)
                sum += note;
            return (Notes.Length > 0) ? sum / Notes.Length : null;
            // return Notes.Average();
        }
        public double? TrouverMax()
        {
            double max = double.MinValue;
            foreach (var note in Notes)
            if (note > max)
                max = note;
            return (Notes.Length > 0) ? max : null;
            // return Notes.Max();
        }
        public double? TrouverMin()
        {
            double min = double.MaxValue;
            foreach (var note in Notes)
            if (note > min)
                min = note;
            return (Notes.Length > 0) ? min : null;
            // return Notes.Min();
        }

    }

    class Formation
    {
        public string Intitule { get; set; }
        public int NbJours { get; set; }
        public List<Stagiaire> Stagiaires { get; set; }

        public Formation(string intitule, int nbJours, Stagiaire[] stagiaires)
        {
            Intitule = intitule;
            NbJours = nbJours;
            Stagiaires = new List<Stagiaire>(stagiaires);
        }

        public double CalculerMoyenneFormation() {
            double sommeDesMoyennes = 0;
            foreach(var stagiaire in Stagiaires)
            {
                sommeDesMoyennes += stagiaire.CalculerMoyenne() ?? 0;
            }
            return sommeDesMoyennes/Stagiaires.Count;
            // return Stagiaires
            //     .Select(s => s.CalculerMoyenne() ?? 0)
            //     .Average();

        }

        public int TrouverIndiceMax() {
            // créer une variable pour mémoriser l'indice du meilleur stagiaire et l'initialiser à -1
            int indiceMeilleurStagiaire = -1;
            double[] moyennes = Stagiaires
                .Select(s => s.CalculerMoyenne() ?? 0)
                .ToArray();
            // pour chaque stagiaire s (avec les indices)
            for (int i=0; i<Stagiaires.Count; ++i)
                // s'il n'y a pas de meilleur stagaire (valeur -1) ou que la moyenne de s est supérieure à celle du meilleur
                if (indiceMeilleurStagiaire == -1 || 
                    moyennes[i] > moyennes[indiceMeilleurStagiaire])
                    // l'indice de s est le meilleur stagiaire
                    indiceMeilleurStagiaire = i;
            // renvoyer l'indice du meilleur
            return indiceMeilleurStagiaire;
            // return Stagiaires
            //     .Select((s,i) => Tuple.Create(i, s.CalculerMoyenne()))
            //     .Aggregate((max, t) => (max.Item2 >= t.Item2) ? max : t)
            //     .Item1;
        }

    }

}